import createParser from "./createParser";
import isError from "./isError";

describe("isError", () => {
  it("correctly determines error", () => {
    expect(
      isError(
        createParser(() => {
          throw new Error("foo");
        })("bar")
      )
    ).toEqual(true);
  });
});
