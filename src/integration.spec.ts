import isError from "./isError";
import anyOf from "./parsers/anyOf";
import manyOf from "./parsers/manyOf";
import optional from "./parsers/optional";
import sequenceOf from "./parsers/sequenceOf";
import string from "./parsers/string";

describe("integration tests", () => {
  it("batmans", () => {
    const parser = sequenceOf(
      manyOf(string("na")),
      manyOf(string(" ")),
      string("batman")
    ).map((v) => v.flat().join(""));
    const result = parser("nananananananana    batman!");
    if (!isError(result)) {
      expect(result.result).toEqual("nananananananana    batman");
    }
  });

  it("handles optional whitespace", () => {
    const whitespaceChar = anyOf(string(" "), string("\t"), string("\n"));
    const whitespace = manyOf(whitespaceChar);
    const optionalWhitespace = optional(whitespace);
    const parser = sequenceOf(
      string("hello"),
      optionalWhitespace,
      string("world")
    ).map((v) => v.flat().join(""));

    const result1 = parser("hello  \t\t  \n   world");
    const result2 = parser("helloworld");
    if (!isError(result1) && !isError(result2)) {
      expect(result1.result).toEqual("hello  \t\t  \n   world");
      expect(result2.result).toEqual("helloworld");
    } else {
      fail("Unexpected error");
    }
  });
});
