import Parser from "./types/parser";
import ParserState from "./types/parserState";
import ParsingFunction from "./types/parsingFunction";

const createParser = <I, O>(
  parsingFunction: ParsingFunction<I, { result: O; index: number }>
): Parser<I, O> => {
  const parser: Parser<I, O> = (input: I) => {
    let parserState: ParserState<I>;

    if (
      typeof input === "object" &&
      typeof ((input as unknown) as ParserState<unknown>).index === "number"
    ) {
      parserState = (input as unknown) as ParserState<I>;
    } else {
      parserState = {
        index: 0,
        input,
      };
    }

    try {
      const result = parsingFunction(parserState);

      return {
        ...parserState,
        ...result,
      };
    } catch (e) {
      let message: string;
      if (e instanceof Error) {
        message = e.message;
      } else if (typeof e === "string") {
        message = e;
      } else {
        throw new Error("Unknown error type received");
      }

      return {
        ...parserState,
        error: message,
      };
    }
  };

  parser.map = (mappingFunction) =>
    createParser((input) => {
      const originalResult = parsingFunction(input);

      return {
        ...originalResult,
        result: mappingFunction(originalResult.result),
      };
    });

  return parser;
};
export default createParser;
