import ParserOutput from "./parserOutput";
import { ErrorState } from "./parserState";

type Parser<I, O> = ((input: I) => ParserOutput<I, O> | ErrorState<I>) & {
  map: <P>(mappingFunction: (input: O) => P) => Parser<I, P>;
};

export default Parser;
