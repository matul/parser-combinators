import { CorrectState } from "./parserState";

export default interface ParserOutput<I, O> extends CorrectState<I> {
  result: O;
}
