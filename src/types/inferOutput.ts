import Parser from "./parser";

type InferOutput<T> = T extends Parser<any, infer O> ? O : never;

export default InferOutput;
