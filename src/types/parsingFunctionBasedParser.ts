import Parser from "./parser";
import ParsingFunction from "./parsingFunction";

type ParsingFunctionBasedParser<
  F extends ParsingFunction<unknown, unknown>
> = F extends ParsingFunction<infer I, infer O> ? Parser<I, O> : never;

export default ParsingFunctionBasedParser;
