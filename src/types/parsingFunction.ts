import ParserState from "./parserState";

type ParsingFunction<I, O> = (input: ParserState<I>) => O;

export default ParsingFunction;
