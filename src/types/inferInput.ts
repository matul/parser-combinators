import Parser from "./parser";

type InferInput<T> = T extends Parser<infer I, any> ? I : never;

export default InferInput;
