export interface CorrectState<I> {
  input: I;
  index: number;
}

export interface ErrorState<I> {
  input: I;
  index: number;
  error: string;
}

type ParserState<I> = CorrectState<I> | ErrorState<I>;

export default ParserState;
