import ParserState, { ErrorState } from "./types/parserState";

const isError = <I>(type: ParserState<I>): type is ErrorState<I> =>
  !!(type as ErrorState<I>).error;

export default isError;
