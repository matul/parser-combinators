import createParser from "../createParser";
import Parser from "../types/parser";

const string = (expectedString: string): Parser<string, string> =>
  createParser<string, string>((state) => {
    if (state.input.startsWith(expectedString, state.index)) {
      return {
        ...state,
        result: expectedString,
        index: state.index + expectedString.length,
      };
    }
    throw new Error(
      `Parse error: expected '${expectedString}' but found '${state.input.substring(
        state.index,
        state.index + expectedString.length
      )}'`
    );
  });

export default string;
