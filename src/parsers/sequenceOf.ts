import createParser from "../createParser";
import isError from "../isError";
import InferOutput from "../types/inferOutput";
import Parser from "../types/parser";

type InferInputs<T> = T extends Parser<infer I, any>[] ? I : never;
type SequenceResult<T> = {
  [K in keyof T]: InferOutput<T[K]>;
};
type SequenceParser<T> = Parser<InferInputs<T>, SequenceResult<T>>;

const sequenceOf = <T extends Array<Parser<any, any>>>(
  ...parsers: [...T]
): SequenceParser<T> =>
  createParser((input) => {
    let state = { ...input, result: ([] as unknown) as SequenceResult<T> };
    for (let i = 0; i < parsers.length; i += 1) {
      const currentState = parsers[i](state);
      if (isError(currentState)) {
        throw new Error(currentState.error);
      }
      state = {
        ...currentState,
        result: [...state.result, currentState.result] as SequenceResult<T>,
      };
    }

    return state;
  });

export default sequenceOf;
