import isError from "../isError";
import sequenceOf from "./sequenceOf";
import string from "./string";

describe("sequenceOf", () => {
  it("handles positive sequence", () => {
    const parser = sequenceOf(string("hello"), string("world"));
    const result = parser("helloworld");
    if (!isError(result)) {
      expect(result.result).toEqual(["hello", "world"]);
    } else {
      fail("Unexpected error");
    }
  });

  it("propagates error correctly", () => {
    const parser = sequenceOf(string("hello"), string("world"));
    const result = parser("hellowurld");
    expect(isError(result)).toBeTruthy();
  });
});
