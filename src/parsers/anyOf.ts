import createParser from "../createParser";
import isError from "../isError";
import Parser from "../types/parser";

type InferUnionOutputs<T> = T extends Parser<any, infer O>[] ? O : never;
type InferUnionInputs<T> = T extends Parser<infer I, any>[] ? I : never;
const anyOf = <T extends Parser<any, any>[]>(
  ...parsers: [...T]
): Parser<InferUnionInputs<T>, InferUnionOutputs<T>> =>
  createParser((input) => {
    for (let i = 0; i < parsers.length; i += 1) {
      const state = parsers[i](input);
      if (!isError(state)) {
        return state;
      }
    }

    throw new Error(`Parse error: expected any of secret parsers`);
  });

export default anyOf;
