import createParser from "../createParser";
import isError from "../isError";
import InferInput from "../types/inferInput";
import InferOutput from "../types/inferOutput";
import Parser from "../types/parser";

type ManyOutput<T> = Array<InferOutput<T>>;
const manyOf = <T extends Parser<any, any>>(
  parser: T
): Parser<InferInput<T>, ManyOutput<T>> =>
  createParser((input) => {
    let currentState = parser(input);
    if (isError(currentState)) {
      throw new Error(currentState.error);
    }
    let state = {
      ...currentState,
      result: ([] as unknown) as ManyOutput<T>,
    };

    while (!isError(currentState)) {
      state = {
        ...currentState,
        result: [...state.result, currentState.result],
      };
      currentState = parser(state);
    }

    return state;
  });

export default manyOf;
