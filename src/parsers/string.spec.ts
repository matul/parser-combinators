import string from "./string";

describe("string", () => {
  it("should parse correct input", () => {
    expect(string("foo")("foo")).toMatchObject({
      result: "foo",
    });
  });

  it("should increment index", () => {
    expect(string("foo")("foo")).toMatchObject({
      index: 3,
    });
  });

  it("should fail on incorrect input", () => {
    expect(string("foo")("barro")).toMatchObject({
      index: 0,
      error: "Parse error: expected 'foo' but found 'bar'",
    });
  });
});
