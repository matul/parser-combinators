import isError from "../isError";
import manyOf from "./manyOf";
import string from "./string";

describe("many", () => {
  it("handles positive sequence", () => {
    const parser = manyOf(string("na")).map((input) => input.join(""));

    const result = parser("nananananananana");
    if (!isError(result)) {
      expect(result.result).toEqual("nananananananana");
    } else {
      fail("Unexpected error");
    }
  });
});
