import createParser from "../createParser";
import isError from "../isError";
import InferInput from "../types/inferInput";
import InferOutput from "../types/inferOutput";
import Parser from "../types/parser";

const optional = <T extends Parser<any, any>>(
  parser: T
): Parser<InferInput<T>, InferOutput<T> | null> =>
  createParser((input) => {
    const result = parser(input);
    if (isError(result)) {
      return {
        ...input,
        result: null,
      };
    }

    return result;
  });

export default optional;
