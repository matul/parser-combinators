import isError from "../isError";
import anyOf from "./anyOf";
import string from "./string";

describe("anyOf", () => {
  const parser = anyOf(string("hello"), string("world"));
  it("works", () => {
    const result1 = parser("hello");
    const result2 = parser("world");

    if (!isError(result1) && !isError(result2)) {
      expect(result1.result).toEqual("hello");
      expect(result2.result).toEqual("world");
    } else {
      fail("Unexpected error");
    }
  });

  it("returns error when none found", () => {
    const result = parser("foo");
    expect(isError(result)).toBeTruthy();
  });
});
