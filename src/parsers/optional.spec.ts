import isError from "../isError";
import optional from "./optional";
import string from "./string";

describe("optional", () => {
  it("just works", () => {
    const parser = optional(string("hello"));
    const result1 = parser("hello");
    const result2 = parser("world");

    if (!isError(result1) && !isError(result2)) {
      expect(result1.result).toEqual("hello");
      expect(result2.result).toEqual(null);
    } else {
      fail("Unexpected error");
    }
  });
});
