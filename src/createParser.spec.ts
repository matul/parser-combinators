import createParser from "./createParser";
import isError from "./isError";

describe("createParser", () => {
  const parsingFunction = () => ({
    index: 1,
    result: "foo",
  });
  it("should pass parsing function return value", () => {
    const parser = createParser(parsingFunction);
    expect(typeof parser).toEqual("function");
    expect(parser("bar")).toMatchObject({
      index: 1,
      result: "foo",
    });
  });

  it("should map returned value", () => {
    const parser = createParser(parsingFunction).map((input) => `${input}bar`);
    const output = parser("baz");
    if (!isError(output)) {
      expect(output.result).toEqual("foobar");
    } else {
      fail("Unexpected parser error");
    }
  });

  it("should wrap input into parser state", () => {
    const mockCallback = jest.fn(parsingFunction);
    const parser = createParser(mockCallback);

    parser("bar");
    expect(mockCallback).toHaveBeenCalledWith({
      index: 0,
      input: "bar",
    });
  });

  it("should convert exception to error state", () => {
    const parser = createParser(() => {
      throw new Error("foo");
    });

    expect(() => parser("bar")).not.toThrowError();
    expect(parser("bar")).toMatchObject({
      index: 0,
      error: "foo",
    });
  });
});
