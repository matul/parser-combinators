module.exports = {
    extends: "airbnb-typescript-prettier",
    plugins: ["unused-imports"],
    rules: {
        "unused-imports/no-unused-imports-ts": "error",
        "unused-imports/no-unused-vars-ts": [
          "warn",
          { "vars": "all", "varsIgnorePattern": "^_", "args": "after-used", "argsIgnorePattern": "^_" }
        ],
        "@typescript-eslint/no-explicit-any": "off"
    }
};